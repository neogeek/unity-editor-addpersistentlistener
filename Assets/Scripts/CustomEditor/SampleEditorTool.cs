using UnityEditor;
using UnityEditor.Events;
using UnityEngine;

public static class SampleEditorTool
{

    [MenuItem("Sample/Add Event")]
    private static void AddEvent()
    {

        var gameObject = GameObject.Find("GameObject");

        var sampleComponent = gameObject.GetComponent<SampleComponent>();

        if (sampleComponent == null)
        {

            sampleComponent = gameObject.AddComponent<SampleComponent>();

        }

        Selection.activeGameObject = gameObject;

        UnityEventTools.AddPersistentListener(sampleComponent.sampleUnityEvent, sampleComponent.HandleEvent);

    }

}
