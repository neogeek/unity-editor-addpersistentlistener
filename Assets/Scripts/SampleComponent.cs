using UnityEngine;
using UnityEngine.Events;

public class SampleComponent : MonoBehaviour
{

    public UnityEvent sampleUnityEvent;

    public void HandleEvent()
    {

        Debug.Log("Event");

    }

}
